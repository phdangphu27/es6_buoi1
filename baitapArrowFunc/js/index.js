const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

const renderColor = () => {
  let contentHTML = "";
  colorList.forEach((color) => {
    contentHTML += `<button class='color-button ${color}'></button>`;
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderColor();

const changeColor = (e) => {
  console.log(e.target.classList);
  if (!e.target.classList[1]) return;

  document.getElementById("house").className =
    "house " + `${e.target.classList[1]}`;
};

document
  .getElementById("colorContainer")
  .addEventListener("click", changeColor);
