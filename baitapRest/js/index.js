const calcAvg1 = () => {
  const toan = +document.getElementById("inpToan").value;
  const ly = +document.getElementById("inpLy").value;
  const hoa = +document.getElementById("inpHoa").value;

  const avg = calcAvg(toan, ly, hoa);
  document.getElementById("tbKhoi1").innerHTML = avg;
};

const calcAvg2 = () => {
  const van = +document.getElementById("inpVan").value;
  const su = +document.getElementById("inpSu").value;
  const dia = +document.getElementById("inpDia").value;
  const english = +document.getElementById("inpEnglish").value;

  const avg = calcAvg(van, su, dia, english);
  document.getElementById("tbKhoi2").innerHTML = avg;
};

const calcAvg = (...scores) => {
  let total = 0;
  scores.forEach((score) => {
    total += score;
  });
  return (total / scores.length).toFixed(2);
};
