const text = document.querySelector(".heading").innerHTML;
const chars = [...text.split(" ").join("")];

const render = () => {
  let contentHTML = "";
  chars.forEach((char) => {
    contentHTML += `<span>${char}</span>`;
  });
  document.querySelector(".heading").innerHTML = contentHTML;
};

render();
